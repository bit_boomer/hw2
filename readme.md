# Лабораторная работа 2  
  
src - исходники.  
FakeMetricGen - perl-скрипт генерации данных.
utility - дополнительные скрипты для удобства демонстрации.
pom.xml - конфигурационный файл для сборки.
MetricAgg.cfg - конфигурация системы для приложения.

# Требования  
1. RPM-based дистрибутив Linux с ядром версии не ниже 3.10 с доступом в интернет.  
2. JDK 1.8  
3. Spark 2.4.7 
4. Ignite 2.9.0
5. Kafka 2.4.1
6. maven  
7. Scala 2.11

# Сборка  
1. Выкачиваем директорию src и файл pom.xml  
2. В директории сборки запускаем   
mvn install  
для тестирования и сборки. Итоговый JAR будет расположен в директории target.  

# Запуск  

1. Запустить сервис Kafka  
2. Запустить master и slave Spark  
3. Запустить приложение  
    ```console
    foo@bar:spark-submit --class 'MetricAggregationApp' --master '<spark master URL>'\
    --deploy-mode cluster \
    <jar-file> <cfg системы> <входной файл с данными> <выходная директория> <масштаб>
    ```

# Генератор данных 
Для удобства проверки предоставлен скрипт генерации тестовых данных.  

1. Выкачать директорию FakeMetricGen.  
4. Находясь в директории FakeMetricGen запустить генератор командой:  
    ```console
    foo@bar:~/fakelog$ perl ./fakeMetric.pl <количество строк> <процент неверных строк> <путь выходного файла>  
    ```
 
## Текущий статус
|Требование лабораторной  |Статус   |
|---|---|
|2.	Program which aggregate raw metrics into selected scale.  Data input format: metricId, timestamp, value  Data output format: metricId, timestamp, scale, value  |  + |
|1. Kafka producer   | +  |
|3. Ignite Native Persistence   | + |
|1. Spark RDD | +  |
  
|Требование репорта  |Статус   |
|---|---|
|1.	IDE agnostic build: Maven, Ant, Gradle, sbt, etc (10 points)| + |
|2.	Unit tests are provided (20 points) | - |
|3.	Code is well-documented (10 points) | + |
|4.	Script for input file generation or calculation setup and control (10 points) | + |
|5.	Working application that corresponds business logic, input/output format and additional Requirements that has been started on cluster (30 points) | + |
|6. The relevant report was prepared (20 points) | + |
